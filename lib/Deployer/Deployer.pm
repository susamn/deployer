package Deployer::Deployer;
use Dancer ':syntax';

our $VERSION = '0.1';

get '/' => sub {
    template 'index';
};

#post '/developer' => sub {
#    template 'developer';
#};

#post '/manager' => sub {
#   template 'manager';
#};

#post '/deployer' => sub {
#    template 'deployer';
#};

true;
