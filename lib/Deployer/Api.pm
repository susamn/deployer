package Deployer::Api;
use Dancer ':syntax';

set serializer => "JSON";

post "/login" => sub{
	my $templt = &login();
	template $templt;
	
};

get "/getCurrentJob" => sub{
	content_type 'application/json';
	return &getCurrentJob();
};

get "/reviewJobs" =>sub {
	content_type 'application/json';
	return &reviewJobs();
};

post "/postCurrentJob" => sub{
	&postCurrentJob();
};


true;

sub login{
	
	my ($username,$password);
	$username=param("username");
	$password=param("password");
	
	if($username eq 'developer@gmail.com' && $password eq "12345"){
		return 'developer';
	}
	elsif($username eq 'manager@gmail.com' && $password eq "12345"){
		return 'manager';
	}
	elsif($username eq 'deployer@gmail.com' && $password eq "12345"){
		return 'deployer';
	}
	else{
		return 'developer';
	}
	
	
	
}


 sub postCurrentJob{
 	my($job,$runStatus); 	
 	my $body = request->body;
 	$job = from_json( $body );
 	$job->{'postDate'}=localtime() unless $job->{'postDate'};
 	$job->{'modifiedDate'}=localtime();
 	mkdir(config->{'data_location'}) unless -d config->{'data_location'};
 	my $filename = config->{'jobs_file'};
	open(my $fh, '>', $filename) or die "Could not open file '$filename' $!";
	print $fh JSON::XS->new->utf8(1)->pretty(1)->encode($job) . "\n";
	close $fh;
   	
 }
 
sub getCurrentJob{
 	my ($job); 
 	mkdir(config->{'data_location'}) unless -d config->{'data_location'};
 	my $filename = config->{'jobs_file'};
	open(my $fh, '<:encoding(UTF-8)', $filename)
  	or die "Could not open file '$filename' $!";
 
	while (my $row = <$fh>) {
	chomp $row;
  	$job = $job . $row; 	
}
	close $fh;
  	return $job;
   	
 }
 
 sub reviewJobs{
 	my $textData = &getCurrentJob();
 	my $jsonData = JSON::XS->new->utf8(1)->pretty(1)->decode($textData);
 	my @data;
 	foreach my $job (@{$jsonData->{'jobs'}}){
 		push @data, $job if $job->{'state'} =~ /Review/;
 	}
 	return JSON::XS->new->utf8(1)->pretty(1)->encode(\@data);
 	
 }
 

 	