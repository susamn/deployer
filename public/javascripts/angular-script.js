angular.module('deployer',[])
.service('jobService',[function(){
    var data={};
    data.jobs=[];
    data.readyForDeployment=false;
    return{
        addJobs:function(job){
            if(job.jobName!=''){
            data.jobs.push(job);}
        },
        removeJobs:function(){

        },
        getData:function(){
            return data;
        },
        getJobs:function(){
            return data.jobs;
        },
        setJobs:function(jobsArg){
            data.jobs=jobsArg;
        },
        setData:function(dataArg){
            data=dataArg;
        },

    };
}])
.service('backendService',['$http',function($http){
    var userData={};
    
        this.getCurrentJob=function(){
        var service = $http.get("/getCurrentJob").success(function(data, status, headers, config){
                userData=data;
                //return userData;
            });
        return service;
        
        };

        this.postCurrentJob=function(dataObject){
        var service = $http.post("/postCurrentJob",dataObject);
        return service;       
        };

        this.reviewJobs=function(dataObject){
        var service = $http.get("/reviewJobs",dataObject);
        return service;       
        };

    


}])
.controller('developerController',['$scope','jobService','backendService',function($scope,jobService,backendService){


    backendService.reviewJobs().then(function(response){
        if(response.data.length > 0){
            $scope.warningInPreviousDeployment=true;
        }
    });
                


    $scope.jobPage='/partials/jobs.html';
    $scope.newDeployment=true;
    $scope.saveJobs=function(){
        //alert($scope.job.jobName);
        // Saving static properties
        $scope.job.id=generateUUID();
        $scope.job.state='Pending';

        jobService.addJobs($scope.job);
        $scope.data=jobService.getData();

        $scope.job={
        'jobName':'',
        'jobDescription':'',
        'host':'',
        'script':'',
        'jobColor':''
    };

    $scope.deleteJob=function(jobId){

        var idx;
        for(i=0;i<=$scope.data.jobs.length;i++){
            var obj = ($scope.data.jobs)[i];
            if(obj.id==jobId){
                idx = i;
                break;
            }
        }
        jobService.getJobs().splice(idx,1);
        $scope.data=jobService.getData();
    };

    $scope.modifyJob=function(jobId){

        alert($scope.job.jobName);
        alert(jobId);
    };

    };
    $scope.publishJobs=function(){
        backendService.postCurrentJob(jobService.getData()).success(function(response){
            successfullJobPost=true;
        
        }).error(function(response){
            unSuccessfullJobPost=true;
        });

       $scope.job={
                'jobName':'',
                'jobDescription':'',
                'host':'',
                'script':'',
                'jobColor':''
                };

    };

    $scope.fetchPreviousDeployment=function(){
        backendService.reviewJobs().then(function(response){
            $scope.data={};
        $scope.data.jobs = response.data;
        $scope.newDeployment=false;
        $scope.oldDeployment=true;
    });

    };


    //$scope.saveJobs($scope.job);
}])
.controller('managerController',['$scope','jobService','backendService',function($scope,jobService,backendService){

   
    backendService.getCurrentJob().then(function(response){
        $scope.data = response.data;
    });

    //$scope.jobPage='/partials/jobs.html';
    $scope.data;
    $scope.selectedJobId="";
    $scope.selectedJobName="Select any job";
    $scope.selectedJobDescription="The description of the selected job will appear here.";
    $scope.jobSelected=false;
    $scope.selectedJobDeclineMessage="None";
    $scope.showDeclineFlag;
    //$scope.
    $scope.showJob=function(jobName,jobDescripton,jobId, declineReason){
        $scope.selectedJobName=jobName;
        $scope.selectedJobDescription=jobDescripton;
        $scope.jobSelected=true;
        $scope.selectedJobId=jobId;
        for(i=0;i<=$scope.data.jobs.length;i++){
            var obj = ($scope.data.jobs)[i];
            if(obj.id==jobId){
                if(obj.state=="Accept"){
                    $scope.selectedJobDeclineMessage = "NA";
                }
                else if(obj.state=="Review"){
                    $scope.selectedJobDeclineMessage = "NA. Sent to develper for review.";

                }
                else{
                    $scope.selectedJobDeclineMessage = obj.declineReason;
                }
                
                break;
            }
        }
        

    };
    $scope.approveJob=function(jobId){
        for(i=0;i<=$scope.data.jobs.length;i++){
            var obj = ($scope.data.jobs)[i];
            if(obj.id==jobId){
                obj.state="Accept";
                obj.declineReason="NA";
                obj.runStatus="Pending";
                break;
            }
        }
        jobService.setData($scope.data);

    };
    $scope.declineJob=function(jobId,selectedJobDeclineMessage){  

        $scope.showDeclineFlag=false;      
        
        for(i=0;i<=$scope.data.jobs.length;i++){
                    var obj = ($scope.data.jobs)[i];
                    if(obj.id==jobId){
                        
                        obj.state="Decline";

                        obj.declineReason=selectedJobDeclineMessage;
                        $scope.selectedJobDeclineMessage=selectedJobDeclineMessage;
                        //$scope.selectedJobDeclineMessage=""; // Resetting for the usage of other jobs
                        break;
                        }
                    }
        
        jobService.setData($scope.data);
        
    };

    $scope.reviewJob=function(jobId){
        for(i=0;i<=$scope.data.jobs.length;i++){
            var obj = ($scope.data.jobs)[i];
            if(obj.id==jobId){
                obj.state="Review";
                break;
            }
        }
        jobService.setData($scope.data);

    };

    $scope.publishJobs=function(){
        jobService.getData().readyForDeployment = $scope.data.readyForDeployment;
        backendService.postCurrentJob(jobService.getData()).success(function(response){
            successfullJobPost=true;
        
    }).error(function(response){
    unSuccessfullJobPost=true;
    });

    };

    $scope.showDeclineBox=function(){
        $scope.showDeclineFlag=true;
    }
    $scope.hideDeclineBox=function(){
        $scope.showDeclineFlag=false;
    }

}])
.controller('deployerController',['$scope','jobService','backendService',function($scope,jobService,backendService){

    backendService.getCurrentJob().then(function(response){
        $scope.data = response.data;
        if($scope.data.readyForDeployment==true){
            $scope.deploymentPanel = "success";
            $scope.deploymentPanelMessage = "Go";
        }
        else{
            $scope.deploymentPanel = "danger";
            $scope.deploymentPanelMessage = "Not Yet";
        }
    });

    //$scope.jobPage='/partials/jobs.html';
    $scope.selectedJobId="";
    $scope.selectedJobName="Select any job";
    $scope.selectedJobDescription="The description of the selected job will appear here.";
    $scope.jobSelected=false;
    //$scope.
    $scope.showJob=function(jobName,jobDescripton,jobId){
        $scope.selectedJobName=jobName;
        $scope.selectedJobDescription=jobDescripton;
        $scope.jobSelected=true;
        $scope.selectedJobId=jobId;

    };

}]);